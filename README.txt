
-- SUMMARY --

The Activity Report module keeps track of Node activity on your site. It creates a table that stores
a row whenever a node is Created, Published, Unpublished, Updated, or Deleted. Since a node cannot be
Published or Unpublished without being Updated too these actions are recoreded as a Publish/Unpublish
activity, and not an update. We also record the node type, language, user and date. It also provides
a way for other modules to add data that is recorded. This module allows this data to be downloaded
in CSV format for a given date range. By default this report returns all rows for the given date
range, but there is a hook provided that lets other modules add custom queries that are available
for the report as well.

-- REQUIREMENTS --

None.

-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.

This module provides one Permission "administer activity report" for people who can access the report page.

-- CONFIGURATION --

Configure user permissions in Administration -> People -> Permissions

Access the report page from Administration -> Reports -> Activity Report

-- CUSTOMIZATION --

This module defines 2 custom hooks: hook_activity_report, and hook_activity_report_query

 - hook_activity_report
    Parameters:
      - $node: The node object whose activity is being recorded
      - $op: The operation that is being recoreded. Allowed values are "insert", "update", "deleted", and "header"

    Functionality:
      This function allows other modules to add extra data to the report table. This is called once from
      the nodeapi passing in the node and op variables for the hook module to work with. This hook
      implementation should return a string. The return data from each module that implements this hook
      is stored as a CSV in the "extra" column of the database.

      This hook should also have a check for when $op is "header". When this is the case it should
      return the text that goes in the header for the returned data point.

    Example:

      function mymodule_activity_report($node, $op) {
        if ($op == 'header') {
          $extra = 'Custom Data';
        }
        else {
          $extra = $node->my_custom_field[0]['value'];
        }
        return $extra;
      }

 - hook_activity_report_query
    Paramters:
      - $report - Name of the report that we are running, or "label" when defining a report
      - $start - Start date of the report as a unixtimestamp
      - $end - End date of the report as a unixtimestamp

    Functionality:
      This function allows other modules to define custom queries to run on the report page. When
      $report is "label", this function must return an array that is used as a radio button option on
      the interface. The key should be the machine readable name, and the Value is the human friendly form.

      When $report is not "label", it will be the machine readable key that was returned by the first
      call to this function. If that is the case, this function should return the results from
      a db_query execution.

    Example:
      // This example comes from the activity_report.module itself. Other modules can define this hook as well.
      function activity_report_activity_report_query($report, $start, $end) {
        $retval = null;
        if ($report == 'label') {
          $retval = array('all' => 'All activity');
        }
        elseif ($report == 'all') {
          $sql = "SELECT l.nid, l.vid, l.type, l.action, l.language, replace(l.title, ',', '') as title, l.uid, u.name, FROM_UNIXTIME(l.timestamp) as timestamp, l.extra
                  FROM {activity_report_log} l
                  LEFT JOIN {users} u on u.uid = l.uid
                  WHERE timestamp BETWEEN %d AND %d
                  ORDER BY l.timestamp;";
          $retval = db_query($sql, $start, $end);
        }
        return $retval;
      }
